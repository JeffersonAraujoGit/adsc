# AUTOR: 		Jefferson Pereira de Araujo
# DATA/HORA DE CRIACAO: 26/11/2013 as 01:20
#
class Empresa
	attr_accessor :razao
	attr_accessor :funcionarios
	def initialize(razao)
		@razao = razao
		@funcionarios = Array.new
	end

	def rh(funcionario = [""])
		@funcionarios.push(funcionario)
	end

end
class Funcionario
	attr_accessor :nome
	attr_accessor :cargo

	def initialize(nome,cargo)
		@nome = nome
		@cargo = cargo
	end
end
class Profissao
	attr_accessor :nome
	attr_accessor :salario
	def initialize(nome = "Trainner",salario = 0.0)
		@nome = nome
		@salario = salario
	end
end

class ProcessamentoEmpresa

	def initialize(empresa)
		@empresa = empresa
		@empFun = @empresa.funcionarios
	end

	def processaFuncionarios

		if @empFun.nil?
			puts "Empresa nao possui funcionarios"
		elsif @empFun.respond_to?("each")
			puts "O\(s\) funcionario\(s\) da empresa #{@empresa.razao} sao :"

			@empFun.each do |func|
				puts "\t ==>#{func.nome}"
			end
			puts "\n"
		else
			puts "Algo errado"
		end
	end
	def relat
		puts "\n"
		@empFun.each do |func|
			puts "O funcionario #{func.nome} exece a profissao de '#{func.cargo.nome}' e recebe atualmente #{func.cargo.salario} reais"
		end
	end
	def contabil
		tot = 0.0
		@empFun.each do |func|
			tot = tot + func.cargo.salario
		end
		puts "Total de despesa(s) com funcionario(s): #{tot}"
	end
end

if __FILE__ == $0
	c = Profissao.new("Analista",1278.5)
	cp = Profissao.new("Analista Pleno",1732.7)
	cpd = Profissao.new()

	fc = Funcionario.new("Jefferson",c)
	fcp = Funcionario.new("Joaquina",cp)
	fcpd = Funcionario.new("Candango",cpd)

	e = Empresa.new("TOTVS")

	e.rh(fc)
	e.rh(fcp)
	e.rh(fcpd)


	p = ProcessamentoEmpresa.new(e)
	p.processaFuncionarios
	p.contabil
	p.relat

end
